This is a simple iOS project to show how to preview the unicode "end of ayah" character dynamically for any number.

Instructions for adding font:
-----------------------------
1. Add the font "me_quran.ttf" to your resources in the xcode project.
2. Open ${PRODUCT_NAME}-Info.plist
3. Add new row with name "Fonts provided by application"
4. Add "me_quran.ttf" font in the list

Or use this tutorial: http://codewithchris.com/common-mistakes-with-adding-custom-fonts-to-your-ios-app/

How it works:
-------------
1. Split the integer into digits
2. Add the "ORNATE LEFT PARENTHESIS" unicode character.
3. Then add the unicode character for the digit (0660, 0661, 0662 ... 0669)
4. Add the "ORNATE RIGHT PARENTHESIS" unicode character.