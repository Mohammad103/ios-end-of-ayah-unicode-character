//
//  EndOfAyahTVC.m
//  EndOfAyah
//
//  Created by 121IslamOnline on 7/16/14.
//  Copyright (c) 2014 121IslamOnline. All rights reserved.
//

#import "EndOfAyahTVC.h"

@interface EndOfAyahTVC ()

@end

@implementation EndOfAyahTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"End of Ayah";
    self.tableView.rowHeight = 80.0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSString *)getEndOfAyahWithVerseNo:(int)verseNo {
    NSMutableArray *digits = [NSMutableArray new];
    while(verseNo > 0){
        NSInteger digit = verseNo % 10; // last digit
        verseNo /= 10; // rest of int
        [digits addObject:[NSString stringWithFormat:@"%ld", (long) digit]];
    }
    
    NSMutableString *endOfAyah = [[NSMutableString alloc] init];
    [endOfAyah appendString:@"\uFD3E"];
    
    for(NSNumber *d in [digits reverseObjectEnumerator]){
        switch([d integerValue]){
            case 0:[endOfAyah appendString:@"\u0660"];  break;
            case 1:[endOfAyah appendString:@"\u0661"];  break;
            case 2:[endOfAyah appendString:@"\u0662"];  break;
            case 3:[endOfAyah appendString:@"\u0663"];  break;
            case 4:[endOfAyah appendString:@"\u0664"];  break;
            case 5:[endOfAyah appendString:@"\u0665"];  break;
            case 6:[endOfAyah appendString:@"\u0666"];  break;
            case 7:[endOfAyah appendString:@"\u0667"];  break;
            case 8:[endOfAyah appendString:@"\u0668"];  break;
            case 9:[endOfAyah appendString:@"\u0669"];  break;
        }
    }
    
    [endOfAyah appendString:@"\uFD3F"];
    return endOfAyah;
}

// ===================================
#pragma mark - Table view data source
// ===================================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 286;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EndOfAyahCell" forIndexPath:indexPath];
    
    cell.textLabel.font = [UIFont fontWithName:@"me_quran" size:40];
    cell.textLabel.text = [self getEndOfAyahWithVerseNo:(int) indexPath.row + 1];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    
    return cell;
}

@end
