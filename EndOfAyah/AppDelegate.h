//
//  AppDelegate.h
//  EndOfAyah
//
//  Created by 121IslamOnline on 7/16/14.
//  Copyright (c) 2014 121IslamOnline. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
